﻿namespace FMF.Toetscie.Options;

public class FileRepositoryOptions
{
    public const string FileRepo = "FileRepo";

    public string FilesFolder { get; set; } = string.Empty;
}