using FMF.Toetscie.Repositories;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace FMF.Toetscie.Controllers;

[Controller]
public class HomeController : Controller
{
    private readonly IFileRepository _files;

    public HomeController(IFileRepository files)
    {
        _files = files;
    }

    [HttpGet("")]
    public async Task<ActionResult> Index() 
        => View(await _files.ListDegreesAsync());

    [HttpGet("/{degreeName}")]
    public async Task<ActionResult> DegreeView(string degreeName)
        => View(await _files.GetDegreeAsync(degreeName));

    [HttpGet("/{degreeName}/{courseName}")]
    public async Task<ActionResult> CourseView(string degreeName, string courseName)
        => View(await _files.GetCourseAsync(degreeName, courseName));

    [HttpGet("/{degreeName}/{courseName}/{fileName}")]
    public async Task<ActionResult> FileView(string degreeName, string courseName, string fileName)
    {
        var (fs, mime) = await _files.GetFileStreamAsync(degreeName, courseName, fileName);
        return File(fs, mime);
    }

    [HttpGet("/Error")] // Our error handler. This is not used in the dev environment.
    public ActionResult Error()
    {
        // Make sure to add exceptions that IExamRepository throws here.
        switch (HttpContext.Features.Get<IExceptionHandlerFeature>()?.Error)
        {
            case ArgumentException:
                HttpContext.Response.StatusCode = 400;
                ViewData["Message"] = "Invalid URI";
                break;
            case KeyNotFoundException:
                HttpContext.Response.StatusCode = 404;
                ViewData["Message"] = "Requested resource not found";
                break;
            default:
                HttpContext.Response.StatusCode = 500;
                ViewData["Message"] = "Internal server error";
                break;
        }
        return View();
    }
}
