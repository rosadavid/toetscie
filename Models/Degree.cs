namespace FMF.Toetscie.Models;

public class Degree
{
    public string? Name { get; set; }
    public List<Course> Courses { get; set; } = new();
    public string? Route { get; set; }
}